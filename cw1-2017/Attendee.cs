﻿/* Attendee.cs
 * 
 * This is the Attendee Class
 * Holds information about the attendee of a conference.
 * 
 * Written by 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    public class Attendee
    {
        // The attendee's reference number, must be in range 40000-60000
        private int attendeeRef;

        public int AttendeeRef
        {
            get
            {
                return attendeeRef;
            }
            set
            {
                if ((value < 40000) || (value > 60000))
                {
                    throw new ArgumentException("AttendeeRef out of range.");
                }
                attendeeRef = value;
            }
        }

        // Attendee's first name, must not be blank
        private string firstName;

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("First Name blank");
                }
                firstName = value;
            }
        }

        //Attendee's second name, must not be blank
        private string secondName;

        public string SecondName
        {
            get
            {
                return secondName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Second Name blank");
                }
                secondName = value;
            }
        }

        // The institution the attendee is a member of, must not be blank
        private string institutionName;

        public string InstitutionName
        {
            get
            {
                return institutionName;
            }
            set
            {
                institutionName = value;
            }
        }

        // The name of the conference the being attended, must not be blank
        private string conferenceName;

        public string ConferenceName
        {
            get
            {
                return conferenceName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Conference Name blank");
                }
                conferenceName = value;
            }
        }

        // The type of registation booked, used to get cost, must be Full, Student or Organiser
        private string registrationType;

        public string RegistrationType
        {
            get
            {
                return registrationType;
            }
            set
            {
                if ((value != "Full") && (value != "Student") && (value != "Organiser"))
                {
                    throw new ArgumentException("Registration Type incorrect");
                }
                registrationType = value;
            }
        }

        // Stores whether or not the attendee has paid
        private bool paid;

        public bool Paid
        {
            get
            {
                return paid;
            }
            set
            {
                paid = value;
            }
        }

        // Stores whether or not the attendee is a presenter, used to get cost and when displaying certificate
        private bool presenter;

        public bool Presenter
        {
            get
            {
                return presenter;
            }
            set
            {
                presenter = value;
            }
        }

        // The title of the paper a presenter presents, must be blank if not a presenter, must not be blank if attendee is a presenter
        private string paperTitle;

        public string PaperTitle
        {
            get
            {
                return paperTitle;
            }
            set
            {
                if ((!presenter) && (value != ""))
                {
                    throw new ArgumentException("Not a presenter");
                }
                paperTitle = value;
            }
        }
        
        
        // Returns the cost for the Attendee to attend the conference according to registration type
        public double GetCost()
        {
            double cost = 0;
            if (registrationType == "Full")
            {
                cost = 500;
            }
            else if (registrationType == "Student")
            {
                cost = 300;
            }

            if (presenter)
            {
                cost *= 0.9;
            }
            return cost;
        }

    }
}
