﻿/* CertificateWindow
 * 
 * Displays a certificate that confirms the Attendee's attendance of the conference.
 * 
 * Written by 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for CertificateWindow.xaml
    /// </summary>
    public partial class CertificateWindow : Window
    {
        public CertificateWindow()
        {
            InitializeComponent();
        }
        
        // Overloaded ShowDialog method to take in reference to Attendee, allowing for the information to be displayed
        public void ShowDialog(ref Attendee attendee)
        {

            Label CertificateLabel = new Label();
            CertificateLabel.Name = "CertificateLabel";
            string certificateString = "";
            if (attendee.Presenter)
            {
                certificateString = String.Format("This is to certify that {0} {1} attended {2} and presented a paper entitled {3}.", attendee.FirstName, attendee.SecondName, attendee.ConferenceName, attendee.PaperTitle);
            }
            else
            {
                certificateString = String.Format("This is to certify that {0} {1} attended {2}.", attendee.FirstName, attendee.SecondName, attendee.ConferenceName);
            }
            CertificateLabel.Content = certificateString;

            Grid.SetColumn(CertificateLabel, 0);
            Grid.SetRow(CertificateLabel, 0);
            CertificateGrid.Children.Add(CertificateLabel);
            base.ShowDialog();
        }

        // Closes window
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
