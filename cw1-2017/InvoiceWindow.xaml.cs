﻿/* InvoiceWindow
 * 
 * Displays an invoice for the Attendee.
 * 
 * Written by 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        public InvoiceWindow()
        {
            InitializeComponent();

        }

        // Overloaded ShowDialog method to take in reference to Attendee, allowing for the information to be displayed
        public void ShowDialog(ref Attendee attendee)
        {

            Label InvoiceLabel = new Label();
            InvoiceLabel.Name = "InvoiceLabel";
            string invoiceString = String.Format("Total payable by {0} {1} from {2} for attending {3} is {4:C2} ", attendee.FirstName, attendee.SecondName, attendee.InstitutionName, attendee.ConferenceName, attendee.GetCost());
            InvoiceLabel.Content = invoiceString;

            Grid.SetColumn(InvoiceLabel, 0);
            Grid.SetRow(InvoiceLabel, 0);
            InvoiceGrid.Children.Add(InvoiceLabel);
            base.ShowDialog();
        }

        // Closes window
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
