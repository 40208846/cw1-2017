﻿/* MainWindow
 * 
 * This window is used to input the attendee's information.
 * An invoice and certificate can be generated.
 * 
 * Written by 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Attendee attendee = new Attendee();

        public MainWindow()
        {
            InitializeComponent();
        }

        // Clears any red borders from TextBoxes
        private void clearErrorHighlight()
        {
            FirstNameTextBox.ClearValue(TextBox.BorderBrushProperty);
            SecondNameTextBox.ClearValue(TextBox.BorderBrushProperty);
            AttendeeRefTextBox.ClearValue(TextBox.BorderBrushProperty);
            InstitutionNameTextBox.ClearValue(TextBox.BorderBrushProperty);
            ConferenceNameTextBox.ClearValue(TextBox.BorderBrushProperty);
            PaperTitleTextBox.ClearValue(TextBox.BorderBrushProperty);

        }

        // Tries to set the properties of the attendee, errors displayed to the user
        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            clearErrorHighlight();
            try
            {
                attendee.FirstName = FirstNameTextBox.Text;

            }
            catch (ArgumentException)
            {
                FirstNameTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("First Name must not be blank");
            }

            try
            {
                attendee.SecondName = SecondNameTextBox.Text;
            }
            catch (ArgumentException)
            {
                SecondNameTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Second Name must not be blank");
            }
            try
            {
                attendee.AttendeeRef = Int32.Parse(AttendeeRefTextBox.Text);
            }
            catch (FormatException)
            {
                AttendeeRefTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Attendee Reference must be a number");
            }
            catch (ArgumentException)
            {
                AttendeeRefTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Attendee Reference must be in the range 40000-60000");
            }
            try
            {
                attendee.InstitutionName = InstitutionNameTextBox.Text;
            }
            catch (ArgumentException)
            {
                InstitutionNameTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Institution Name must not be blank");
            }
            try
            {
                attendee.ConferenceName = ConferenceNameTextBox.Text;
            }
            catch (ArgumentException)
            {
                ConferenceNameTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Conference Name must not be blank");
            }

            attendee.RegistrationType = RegistrationTypeComboBox.Text;
            attendee.Paid = PaidCheckBox.IsChecked.Value;
            attendee.Presenter = PresenterCheckBox.IsChecked.Value;

            try
            {
                attendee.PaperTitle = PaperTitleTextBox.Text;
            }
            catch (ArgumentException)
            {
                PaperTitleTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Only presenters can have Paper Titles");
            }
        }

        // Clears all inputs i.e. TextBoxes and CheckBoxes
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            clearErrorHighlight();
            FirstNameTextBox.Text = "";
            SecondNameTextBox.Text = "";
            AttendeeRefTextBox.Text = "";
            InstitutionNameTextBox.Text = "";
            ConferenceNameTextBox.Text = "";
            PaidCheckBox.IsChecked = false;
            PresenterCheckBox.IsChecked = false;
            PaperTitleTextBox.Text = "";
        }

        // Loads stored attendee details
        private void GetButton_Click(object sender, RoutedEventArgs e)
        {
            FirstNameTextBox.Text = attendee.FirstName;
            SecondNameTextBox.Text = attendee.SecondName;
            AttendeeRefTextBox.Text = attendee.AttendeeRef.ToString();
            InstitutionNameTextBox.Text = attendee.InstitutionName;
            ConferenceNameTextBox.Text = attendee.ConferenceName;
            PaidCheckBox.IsChecked = attendee.Paid;
            PresenterCheckBox.IsChecked = attendee.Presenter;
            PaperTitleTextBox.Text = attendee.PaperTitle;
        }

        // Displays invoice for attendee
        private void InvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            InvoiceWindow invoiceWindow = new InvoiceWindow();
            invoiceWindow.ShowDialog(ref attendee);
        }

        // Displays certificate for attendee
        private void CertificateButton_Click(object sender, RoutedEventArgs e)
        {
            CertificateWindow certificateWindow = new CertificateWindow();
            certificateWindow.ShowDialog(ref attendee);
        }
    }
}
